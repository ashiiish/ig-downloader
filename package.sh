#!/usr/bin/env bash

package() {
    pushd web-ext
    mkdir -p ../build
    zip -r -FS ../build/ig-downloader.zip *
    popd
}

clean() {
    rm -rf build/*.zip
}

main() {
    function=$1
    $1
}

if [[ ${1} == "-h" ]]; then
    echo "./package.sh [clean|package]"
    exit 0
fi
main ${@}
