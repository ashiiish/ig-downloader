$(function(){
    let css = `
        <style type="text/css">
        .btn-ctr {
            display: inline-block;
            background-color: transparent;
            border: none;
            cursor: pointer;
            padding: 8px;
        }
        .dl-btn {
            display: block;
            overflow: hidden;
            text-indent: 110%;
            white-space: nowrap;
        }
        .coreSpriteDownload {
            background-image: url(${chrome.runtime.getURL('/res/download-512.png')});
            height: 24px;
            width: 24px;
            background-position: center center;
            background-size: cover;
        }
        </style>`;
    $(css).appendTo('head');

    function addDLButton() {
        // Extract page locations
        /// The article container for the image/video and comments
        let ig_view = $('body div article'); // or 'body div div div div div div div article
        if (ig_view === undefined) return;

        /// The call to action section
        let action_section = ig_view.find('section')[0];

        // Extract the image/video link
        let item = ig_view.find('img')[1];
        if (item === undefined) {
            /// if failed to extract image link, try video
            item = ig_view.find('video')[0];
        }
        let image_src = item.src;

        // if image_src is not found, we exit
        if (image_src === undefined) {
            console.log("ig-downloader: image/video source not found.");
            return;
        }

        // else we append a download button
        let dlBtnHtml = `
            <a class="btn-ctr" href="${image_src}" role="button">
                <span class="dl-btn coreSpriteDownload">Download</span>
            </a>`;

        if($('.btn-ctr').length === 0) {
            // we only append a download button, if one hasn't been appended before
            action_section.appendChild($(dlBtnHtml)[0]);
        }
    }

    $(document).on('click', function(e) {
        addDLButton();
    });
    $(document).on('load', function(e) {
        addDLButton();
    });
});
